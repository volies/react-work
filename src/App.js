import React from 'react';
import {
    BrowserRouter as Router,
    Switch,
    Route,
    Redirect
} from 'react-router-dom';
import {useSelector, useDispatch} from 'react-redux';
import styled, {createGlobalStyle} from 'styled-components'
import Auth from './pages/Auth';
import Posts from './pages/Posts';
import Single from './pages/Single';
import {setUser} from "./store/auth/actions";

const GlobalStyle = createGlobalStyle`
 * {
    box-sizing: border-box;
  }
  
  html{
    min-height: 100vh
  }
  
  body {
    margin: 0;
    padding: 0;
    min-height: 100vh;
    font-family: 'sans-serif', 'sans';
  }
  
  #root {
    min-height: 100vh;
  }
`;
const Main = styled.div`
  min-height: 100vh;
  background-color: #eeeeee;
`;

export default function App() {
    const user = useSelector(state => state.auth.user);
    const dispatch = useDispatch();
    let auth = Boolean(user);

    if (!user && localStorage.getItem('user')) {
        const data = JSON.parse(localStorage.getItem('user'));

        dispatch(setUser(data));

        auth = Boolean(data);
    }

    return (
        <>
            <GlobalStyle/>
            <Router>
                <Main>
                    <Switch>
                        <Route exact path="/">
                            {auth ? <Posts/> : <Redirect to='/auth' />}
                        </Route>
                        <Route path="/auth">
                            {auth ? <Redirect to='/' /> : <Auth />}
                        </Route>
                        <Route path="/post/:id">
                            {auth ? <Single/> : <Redirect to='/auth' />}
                        </Route>
                    </Switch>
                </Main>
            </Router>
        </>
    );
};
