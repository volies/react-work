export const SET_POSTS = 'SET_POSTS';
export const LOAD_POSTS = 'LOAD_POSTS';
export const SET_PAGE = 'SET_PAGE';
export const SET_LOAD_MORE = 'SET_LOAD_MORE';

export const setPosts = (posts) => ({
    type: SET_POSTS,
    payload: posts
});

export const loadPosts = (posts) => ({
    type: LOAD_POSTS,
    payload: posts
});

export const setPage = (page) => ({
    type: SET_PAGE,
    payload: page
});

export const setLoadMore = (value) => ({
    type: SET_LOAD_MORE,
    payload: value
});
