import {SET_POSTS, LOAD_POSTS, SET_PAGE, SET_LOAD_MORE} from './actions';

const defaultState = {
    posts: null,
    page: 1,
    loadMore: false,
    limit: 3
}

export const postsReducers = (state = defaultState, action) => {
    switch(action.type) {
        case SET_POSTS: return {...state, posts: action.payload}
        case LOAD_POSTS: return {...state, posts: [...state.posts, ...action.payload]}
        case SET_PAGE: return {...state, page: action.payload}
        case SET_LOAD_MORE: return {...state, loadMore: action.payload}
    }

    return state;
}
