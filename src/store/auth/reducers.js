import {SET_USER} from './actions';

const defaultState = {
    user: null
}

export const authReducers = (state = defaultState, action) => {
    switch(action.type) {
        case SET_USER: return {...state, user: action.payload};
    }

    return state;
}
