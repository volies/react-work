import {combineReducers} from 'redux';
import {authReducers} from './auth/reducers';
import {postsReducers} from './posts/reducers';

export default combineReducers({
    auth: authReducers,
    posts: postsReducers
});
