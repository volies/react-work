import React, {useState, useEffect} from 'react';
import styled, {css} from 'styled-components';
import axios from 'axios';
import {useSelector, useDispatch} from 'react-redux';
import {setUser} from '../store/auth/actions';
import { useHistory } from 'react-router-dom';

const Center = styled.div`
    display: flex;
    align-items: center;
    justify-content: center;
    height: 100vh;
`;
const Form = styled.div`
    width: 100%;
    max-width: 500px;
    background-color: #ffffff;
    padding: 32px;
    border-radius: 6px;
    
    ${(props) => props.load && css`
        opacity: 0.5;
        pointer-events: none;
    `}
`;
const Title = styled.div`
    text-align: center;
    font-width: 400;
    font-size: 24px;
    margin-bottom: 16px;
`;
const Wrapper = styled.div`
    margin-top: 16px;
    display: flex;
`;
const Input = styled.input`
    width: 100%;
    height: 38px;
    border: 1px solid #ccc;
    border-radius: 2px;
    padding: 0 10px;
    
    &:focus {
        outline: none;
        border-color: #aaa;
    }
    &::placeholder {
        color: #aaa;
    }
`;
const Submit = styled.button`
    width: 100%;
    height: 48px;
    border: none;
    color: #000;
    font-size: 18px;
    border-radius: 2px;
    cursor: pointer;
    background: #ccc;
    outline: none;
       
    &:disabled {
      pointer-events: none;
      background: #eee;
      color: #ddd;
    }
    &:active {
      background: #eee;
    }
`;

const data = {
    login: "test",
    password: "done",
    id: 2
};

export default function Auth() {
    const dispatch = useDispatch();
    const history = useHistory();
    const user = useSelector(state => state.auth.user);
    const [login, setLogin] = useState('test');
    const [password, setPassword] = useState('done');
    const [pass, setPass] = useState(false);
    const [load, setLoad] = useState(false);

    const onChangeLogin = event => setLogin(event.target.value);
    const onChangePassword = event => setPassword(event.target.value);
    const checkPass = () => setPass(login === data.login && password === data.password);
    const getUserDate = () => {
        const asyncLoad = () => async (asyncDispatch) => {
            setLoad(true);

            const response = await axios.get(`https://jsonplaceholder.typicode.com/users?id=${data.id}`);

            /* Типо грузится долго */
            await new Promise((resolve) => {
                setTimeout(() => {
                    resolve();
                }, 2000);
            });

            asyncDispatch(setUser(response.data[0]));
            localStorage.setItem('user', JSON.stringify(response.data[0]));
            setLoad(false);
            history.push('/');
        }

        dispatch(asyncLoad());
    };

    useEffect(() => {
        if (user) {
            history.push('/');
        } else if (localStorage.getItem('user')) {
            const data = JSON.parse(localStorage.getItem('user'));

            if (data) {
                dispatch(setUser(data));
                history.push('/');
            }
        }
    }, []);

    useEffect(checkPass);

    return (
        <Center>
            <Form load={load}>
                <Title>Вход</Title>
                <Wrapper>
                    <Input placeholder="Логин (test)" value={login} onChange={onChangeLogin}/>
                </Wrapper>
                <Wrapper>
                    <Input placeholder="Пароль (done)" value={password} onChange={onChangePassword}/>
                </Wrapper>
                <Wrapper>
                    <Submit disabled={!pass} onClick={getUserDate}>{load ? '...' : 'Войти'}</Submit>
                </Wrapper>
            </Form>
        </Center>
    )
}
