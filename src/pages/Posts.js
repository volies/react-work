import React, {useEffect, useState} from 'react';
import Post from '../components/Post';
import Header from '../components/Header';
import styled from 'styled-components';
import axios from 'axios';
import {useDispatch, useSelector} from 'react-redux';
import {setPosts, loadPosts, setPage, setLoadMore} from '../store/posts/actions';

const Wrapper = styled.div`
    display: flex;
    flex-direction: column;
    align-items: center;
`;
const Content = styled.div`
    display: flex;
    flex-wrap: wrap;
    margin: 86px -20px 0;
    max-width: 1280px;
    width: 100%;
`;
const Item = styled.div`
    width: 33.3%;
    padding: 0 20px;
    margin-bottom: 40px;
    text-decoration: none;
    color: inherit;
`;
const More = styled.button`
    border: none;
    width: 320px;
    height: 48px;
    background: #ddd;
    cursor: pointer;
    margin-bottom: 20px;
`;

export default function Posts() {
    const dispatch = useDispatch();
    const {id} = useSelector(state => state.auth.user);
    const {posts, page, loadMore, limit} = useSelector(state => state.posts);
    const [loadMoreLoad, setLoadMoreLoad] = useState(false);
    const toLoadPosts = () => {
        const load = () => async (asyncDispatch) => {
            const response = await axios.get(`http://jsonplaceholder.typicode.com/posts?_page=${page}&_limit=${limit}&userId=${id}`);

            /* Типо грузится долго */
            await new Promise((resolve) => {
                setTimeout(() => {
                    resolve();
                }, 500);
            });

            if (posts && posts.length) {
                asyncDispatch(loadPosts(response.data));
            } else {
                asyncDispatch(setPosts(response.data));
            }

            asyncDispatch(setLoadMore(response.data.length === limit));
            setLoadMoreLoad(false)
        }

        setLoadMoreLoad(true);
        dispatch(load());
    }
    const clickMore = () => {
        dispatch(setPage(page + 1));
    };

    useEffect(() => {
        if (!posts) {
            toLoadPosts();
        } else if (posts.length < page * limit) {
            toLoadPosts();
        }
    }, [page]);

    const Items = () => {
        if (!posts) {
            return <h2>Загрузка...</h2>
        }

        return Array.from(posts).map((post) =>
            <Item key={post.id}>
                <Post post={post}/>
            </Item>);
    };

    return (
        <Wrapper>
            <Header/>

            <Content>
                <Items/>
            </Content>
            {loadMore && <More onClick={clickMore} disabled={loadMoreLoad}>{loadMoreLoad ? '...' : 'Загрузить еще'}</More>}

        </Wrapper>
    )
}
