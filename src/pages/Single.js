import React, {useState, useEffect} from 'react';
import Header from '../components/Header';
import styled from 'styled-components';
import {Link, useParams} from 'react-router-dom';
import axios from 'axios';

const Wrapper = styled.div`
    display: flex;
    flex-direction: column;
    align-items: center;
`;
const Content = styled.div`
    display: flex;
    flex-wrap: wrap;
    margin: 86px -20px 0;
    max-width: 1280px;
    width: 100%;
`;
const Main = styled.div`
    transition: border 0.3s;
    width: 100%;
    height: 100%;
    background-color: #ffffff;
    border-radius: 2px;
    padding: 20px;
    display: block;
    color: inherit;
    text-decoration: none;
`;
const Back = styled(Link)`
    font-size: 14px;
    line-height: 1.7;
    margin-top: 24px;
    text-decoration: none;
    color: inherit;
    border-bottom: 1px solid;
    margin-bottom: 54px;
    display: inline-block;
`;
const Title = styled.div`
    font-weight: 600;
    font-size: 20px;
`;
const Text = styled.div`
    font-size: 14px;
    line-height: 1.7;
    margin-top: 24px;
`;

export default function Single() {
    const {id} = useParams();
    const [post, setPost] = useState(null);
    const getPost = async() => {
        const response = await axios.get(`http://jsonplaceholder.typicode.com/posts?id=${id}`);

        setPost(response.data[0]);
    }

    useEffect(getPost, []);

    const PostContent = () => {
        if (post) {
            return (<>
                <Title>{post.title} </Title>
                <Text>{post.body}</Text>
            </>)
        } else {
            return <h2>Загрузка...</h2>
        }
    }

    return (
        <Wrapper>
            <Header/>

            <Content>
                <Main>
                    <Back to="/">Вернуться к постам</Back>
                    <PostContent/>
                </Main>
            </Content>
        </Wrapper>
    )
}
