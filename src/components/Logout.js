import React from 'react';
import styled from 'styled-components';
import {useDispatch} from 'react-redux';
import {setUser} from '../store/auth/actions';

const Button = styled.div`
    cursor: pointer;
`;

export default function Logout() {
    const dispatch = useDispatch();
    const onLogout = () => {
        dispatch(setUser(null));
        localStorage.removeItem('user');
    }

    return (
        <Button onClick={onLogout}>
            <img src="/images/logout.svg" alt="" width="24"/>
        </Button>
    )
}
