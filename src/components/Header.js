import React from 'react';
import styled from 'styled-components';
import UserInfo from '../components/UserInfo';
import Logout from '../components/Logout';

const Main = styled.div`
    width: 100%;
    background-color: #ffffff;
    padding:  6px 30px;
    display: flex;
    justify-content: flex-end;
    align-items: center;
`;
const Wrapper = styled.div`
    margin-left: 48px;
`;

export default function Header() {

    return (
        <Main>
            <UserInfo/>

            <Wrapper>
                <Logout />
            </Wrapper>
        </Main>
    )
}
