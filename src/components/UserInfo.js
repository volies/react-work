import React from 'react';
import styled from 'styled-components';
import {useSelector} from 'react-redux';

const Main = styled.div`
    display: flex;
    align-items: center;
`;

const Avatar = styled.div`
    display: flex;
    align-items: center;
    justify-content: center;
    background-color: #ccc;
    width: 36px;
    height: 36px;
`;

const Name = styled.div`
    margin-left: 12px;
    font-size: 14px;
`;

export default function UserInfo() {
    const user = useSelector(state => state.auth.user);

    return (
        <Main>
            {user && user.name &&
                <>
                    <Avatar>{user.name.slice(0, 1)}</Avatar>
                    <Name>{user.name}</Name>
                </>
            }
        </Main>
    )
}
