import React from 'react';
import styled from 'styled-components';
import {Link} from 'react-router-dom';

const Main = styled(Link)`
    transition: border 0.3s;
    width: 100%;
    height: 100%;
    background-color: #ffffff;
    border-radius: 2px;
    padding: 20px;
    display: block;
    color: inherit;
    text-decoration: none;
    border: 2px solid transparent;
    
    &:hover {
      border-color: #ccc;
    }
`;
const Title = styled.div`
    font-weight: 600;
    font-size: 20px;
`;

const Text = styled.div`
    font-size: 14px;
    line-height: 1.7;
    margin-top: 24px;
`;

export default function Post({post}) {
    return (
        <Main to={`/post/${post.id}`}>
            <Title>{post.title}</Title>
            <Text>{post.body}</Text>
        </Main>
    )
}
